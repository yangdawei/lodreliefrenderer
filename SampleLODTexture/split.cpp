#include "split.h"

using namespace std;

void downSampleByTwo(Image1f &output, const Image1f &input)
{
	assert(&output != &input);

	int originH = input.height();
	int originW = input.width();
	assert((originH % 2) == 0);
	assert((originW % 2) == 0);
	int newH = originH / 2;
	int newW = originW / 2;

	output.resize(newW, newH);

	for (int x = 0; x < newW; x++)
	{
		for (int y = 0; y < newH; y++)
		{
			float sum = 0;
			for (int k = 0; k < 4; k++)
			{
				int xOff = k % 2;
				int yOff = k / 2;
				float val = input.get(x * 2 + xOff, y * 2 + yOff);
				sum += val;
			}
			float result = sum / 4;
			output.set(x, y, result);
		}
	}
}

void computeError(Image1f &error, const Image1f &origin, const Image1f &downsampled)
{
	int stride = origin.width() / downsampled.width();
	assert(stride == origin.height() / downsampled.height());
	assert(origin.width() % downsampled.width() == 0);
	assert(origin.height() % downsampled.height() == 0);

	error.resize(downsampled.width(), downsampled.height());

	for (int x = 0; x < downsampled.width(); x++)
	{
		for (int y = 0; y < downsampled.height(); y++)
		{
			float maxError = 0.0;
			float val = downsampled.get(x, y);
			for (int xOff = 0; xOff < stride; xOff++)
			{
				for (int yOff = 0; yOff < stride; yOff++)
					maxError = fmax(maxError, fabs(val - origin.get(x * stride + xOff, y * stride + yOff)));
			}
			error.set(x, y, maxError);
		}
	}
}
