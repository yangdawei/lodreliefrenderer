#include "../include/Image.hpp"

void downSampleByTwo(Image1f &output, const Image1f &input);
void computeError(Image1f &error, const Image1f &origin, const Image1f &downsampled);
