#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cmath>
#include <vector>

#include "../include/Image.hpp"
#include "../include/ImageHelper.h"
#include "../include/glm/glm.hpp"
#include "../include/lodepng/lodepng.h"

#include "split.h"

using namespace std;

#define STEP 512
#define TEX_STEP 512

void drawBall(Image1f &image, int left, int top, int length)
{
	for (int u = 0; u < length; u++)
	{
		for (int v = 0; v < length; v++)
		{
			float x = u / float(length) - 0.5f;
			float y = v / float(length) - 0.5f;
			float z = sqrtf(fmax(0.75f - x * x - y * y, 0.f));
			image.set(u + left, v + top, z - 0.5f);
		}
	}
}

template <typename T>
void drawImage(Image<T> &image, const Image<T> &source, int left, int top, int width, int height)
{
	Image<T> newSource = source;
	newSource.stretch(width, height);
	for (int u = 0; u < width; u++)
	{
		for (int v = 0; v < height; v++)
		{
			image.set(u + left, v + top, newSource.get(u, v));
		}
	}
}

Image1f randomImage(size_t width, size_t height)
{
	Image1f image(width, height);
	for (size_t x = 0; x < width; x++)
	{
		for (size_t y = 0; y < height; y++)
		{
			image.set(x, y, rand() / (float)RAND_MAX);
		}
	}
	return image;
}

Image1f pngToHeightMap(const char *filename)
{
	vector<unsigned char> data;
	unsigned int width, height;
	unsigned error = lodepng::decode(data, width, height, filename);
	if (error != 0)
		printf("Decode error: %s\n", lodepng_error_text(error));
	Image1f ret(width, height);
	unsigned idx = 0;
	unsigned step = data.size() / width / height;
	for (unsigned x = 0; x < width; x++)
	{
		for (unsigned y = 0; y < height; y++)
		{
			ret.set(x, y, float(data[idx]) / float(255));
			idx += step;
		}
	}

	return ret;
}

Image<glm::vec3> pngToColor(const char *filename)
{
	vector<unsigned char>data;
	unsigned int width, height;
	unsigned error = lodepng::decode(data, width, height, filename);
	if (error != 0)
		printf("Decode error: %s\n", lodepng_error_text(error));
	Image<glm::vec3> ret(width, height);
	unsigned idx = 0;
	unsigned step = data.size() / width / height;
	for (unsigned x = 0; x < width; x++)
	{
		for (unsigned y = 0; y < height; y++)
		{
			ret.set(x, y, glm::vec3(
				float(data[idx + 0]) / float(255),
				float(data[idx + 1]) / float(255),
				float(data[idx + 2]) / float(255)));
			idx += step;
		}
	}

	return ret;
}

void genTest2()
{
	Image1f node0(3 * STEP, 4 * STEP);
	Image1f stone = pngToHeightMap("../data/g06_p.png");

	for (unsigned x = 0; x < stone.width(); x++)
	{
		for (unsigned y = 0; y < stone.height(); y++)
			stone.get(x, y) = stone.get(x, y) / 30 + 0.1f;
	}

	drawImage(node0, stone, STEP, 0, STEP, STEP);
	drawImage(node0, stone, 0, STEP, STEP, STEP);
	drawImage(node0, stone, STEP, STEP, STEP, STEP);
	drawImage(node0, stone, 2 * STEP, STEP, STEP, STEP);
	drawImage(node0, stone, STEP, 2 * STEP, STEP, STEP);
	drawImage(node0, stone, STEP, 3 * STEP, STEP, STEP);

	Image1f node1(2 * STEP, 4 * STEP);
	drawImage(node1, stone, 0, 2 * STEP, 2 * STEP, 2 * STEP);

	Image1f node2(4 * STEP, 4 * STEP);
	drawImage(node2, stone, 0, 0, 2 * STEP, 2 * STEP);
	drawImage(node2, stone, 0, 2 * STEP, 2 * STEP, 2 * STEP);
	drawImage(node2, stone, 2 * STEP, 2 * STEP, 2 * STEP, 2 * STEP);

	Image1f node3(4 * STEP, 4 * STEP);
	drawImage(node3, stone, 0, 0, 2 * STEP, 2 * STEP);
	drawImage(node3, stone, 0, 2 * STEP, 2 * STEP, 2 * STEP);

	Image1f node456789(4 * STEP, 4 * STEP);
	drawImage(node456789, stone, 0, 0, 4 * STEP, 4 * STEP);
	node0.saveRaw("../data/stone/0.dat");
	node1.saveRaw("../data/stone/1.dat");
	node2.saveRaw("../data/stone/2.dat");
	node3.saveRaw("../data/stone/3.dat");
	for (int i = 4; i <= 9; i++)
	{
		char buffer[4096];
		sprintf(buffer, "../data/stone/%d.dat", i);
		node456789.saveRaw(buffer);
	}

	Image<glm::vec3> texture(3 * TEX_STEP, 4 * TEX_STEP);
	Image<glm::vec3> normal = pngToColor("../data/g06_n.png");

	Image<glm::vec3> color(normal.width(), normal.height());
	for (unsigned x = 0; x < color.width(); x++)
	{
		for (unsigned y = 0; y < color.height(); y++)
		{
			glm::vec3 n = normal.get(x, y) * 2.f - glm::vec3(1.f);
			float intensity = glm::dot(n, glm::vec3(0, 0, 1));
			if (intensity < 0)
				intensity = 0;
			color.set(x, y, glm::vec3(intensity));
		}
	}

	color = normal;


	drawImage<glm::vec3>(texture, color, TEX_STEP, 0, TEX_STEP, TEX_STEP);
	drawImage<glm::vec3>(texture, color, 0, TEX_STEP, TEX_STEP, TEX_STEP);
	drawImage<glm::vec3>(texture, color, TEX_STEP, TEX_STEP, TEX_STEP, TEX_STEP);
	drawImage<glm::vec3>(texture, color, 2 * TEX_STEP, TEX_STEP, TEX_STEP, TEX_STEP);
	drawImage<glm::vec3>(texture, color, TEX_STEP, 2 * TEX_STEP, TEX_STEP, TEX_STEP);
	drawImage<glm::vec3>(texture, color, TEX_STEP, 3 * TEX_STEP, TEX_STEP, TEX_STEP);

	texture.saveRaw("../data/stone/texture.dat");

	FILE *info = fopen("../data/stone/info.txt", "w");
	fprintf(info, "%d %d\n", 3 * TEX_STEP, 4 * TEX_STEP);
	fprintf(info, "1 2 0 3   %d %d %d %d %.10f\n", STEP, 2 * STEP, 3 * STEP, 4 * STEP, 0.00390625);
	fprintf(info, "0 0 4 0   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 2 * STEP, 4 * STEP, 0.001953125);
	fprintf(info, "5 0 6 7   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 4 * STEP, 4 * STEP, 0.001953125);
	fprintf(info, "8 0 9 0   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 4 * STEP, 4 * STEP, 0.001953125);
	for (int i = 0; i < 6; i++)
		fprintf(info, "0 0 0 0   0   0  %d %d %.10f\n", 4 * STEP, 4 * STEP, 0.0009765625);

	fclose(info);
}

void genTest1()
{
	Image1f node0(3 * STEP, 4 * STEP);

	drawBall(node0, STEP, 0, STEP);
	drawBall(node0, 0, STEP, STEP);
	drawBall(node0, STEP, STEP, STEP);
	drawBall(node0, 2 * STEP, STEP, STEP);
	drawBall(node0, STEP, 2 * STEP, STEP);
	drawBall(node0, STEP, 3 * STEP, STEP);

	Image1f node1(2 * STEP, 4 * STEP);
	drawBall(node1, 0, 2 * STEP, 2 * STEP);

	Image1f node2(4 * STEP, 4 * STEP);
	drawBall(node2, 0, 0, 2 * STEP);
	drawBall(node2, 0, 2 * STEP, 2 * STEP);
	drawBall(node2, 2 * STEP, 2 * STEP, 2 * STEP);

	Image1f node3(4 * STEP, 4 * STEP);
	drawBall(node3, 0, 0, 2 * STEP);
	drawBall(node3, 0, 2 * STEP, 2 * STEP);

	Image1f node456789(4 * STEP, 4 * STEP);
	drawBall(node456789, 0, 0, 4 * STEP);

	node0.saveRaw("../data/test1/0.dat");
	node1.saveRaw("../data/test1/1.dat");
	node2.saveRaw("../data/test1/2.dat");
	node3.saveRaw("../data/test1/3.dat");

	for (int i = 4; i <= 9; i++)
	{
		char buffer[4096];
		sprintf(buffer, "../data/test1/%d.dat", i);
		node456789.saveRaw(buffer);
	}

	Image<glm::vec3> texture(3 * TEX_STEP, 4 * TEX_STEP);

	auto genRandFloat = []()
	{
		return rand() / float(RAND_MAX);
		// return 0.8f;
	};

	for (size_t x = 0; x < 3 * TEX_STEP; x++) for (size_t y = 0; y < 4 * TEX_STEP; y++)
		texture.set(x, y, glm::vec3(genRandFloat(), genRandFloat(), genRandFloat()));

	for (size_t x = TEX_STEP; x < 2 * TEX_STEP; x++)
	{
		for (size_t y = 0; y < TEX_STEP; y++)
			texture.set(x, y, glm::vec3(genRandFloat(), 0, 0));
		for (size_t y = TEX_STEP; y < 2 * TEX_STEP; y++)
			texture.set(x, y, glm::vec3(0, genRandFloat(), 0));
		for (size_t y = 2 * TEX_STEP; y < 3 * TEX_STEP; y++)
		{
			float f = genRandFloat();
			texture.set(x, y, glm::vec3(0, f, f));
		}
		for (size_t y = 3 * TEX_STEP; y < 4 * TEX_STEP; y++)
		{
			float f = genRandFloat();
			texture.set(x, y, glm::vec3(f, 0, f));
		}
	}

	for (size_t y = TEX_STEP; y < 2 * TEX_STEP; y++)
	{
		for (size_t x = 0; x < TEX_STEP; x++)
			texture.set(x, y, glm::vec3(0, 0, genRandFloat()));
		for (size_t x = 2 * TEX_STEP; x < 3 * TEX_STEP; x++)
		{
			float f = genRandFloat();
			texture.set(x, y, glm::vec3(f, f, 0));
		}
	}

	texture.saveRaw("../data/test1/texture.dat");
	// texture.saveRaw("../data/test2/texture.dat");

	FILE *info = fopen("../data/test1/info.txt", "w");
	fprintf(info, "%d %d\n", 3 * TEX_STEP, 4 * TEX_STEP);
	fprintf(info, "1 2 0 3   %d %d %d %d %.10f\n", STEP, 2 * STEP, 3 * STEP, 4 * STEP, 0.00390625);
	fprintf(info, "0 0 4 0   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 2 * STEP, 4 * STEP, 0.001953125);
	fprintf(info, "5 0 6 7   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 4 * STEP, 4 * STEP, 0.001953125);
	fprintf(info, "8 0 9 0   %d %d %d %d %.10f\n", 2 * STEP, 2 * STEP, 4 * STEP, 4 * STEP, 0.001953125);
	for (int i = 0; i < 6; i++)
		fprintf(info, "0 0 0 0   0   0  %d %d %.10f\n", 4 * STEP, 4 * STEP, 0.0009765625);

	fclose(info);
}

void genTest3()
{
	Image1f heightMapStone = pngToHeightMap("../data/g06_p.png");
	float scale = 4.f;

	for (int i = 0; i < heightMapStone.width(); i++)
	{
		for (int j = 0; j < heightMapStone.height(); j++)
			heightMapStone.set(i, j, heightMapStone.get(i, j) / scale + 0.001f);
	}

	heightMapStone.saveRaw("../data/teapot/0.dat");
	Image<glm::vec3> colorStone = pngToColor("../data/teapot/default.png");
	colorStone.saveRaw("../data/teapot/texture.dat");
}

int main()
{
	const char *inputHeightPng = "../data/g06_p.png";
	const char *outputFolder = "../output/";
	int height = 5;

	Image1f heightImg = pngToHeightMap(inputHeightPng);
	Image1f originImg = heightImg;
	Image1f outputImg, errorImg;
	char name[4096];

	for (int i = 0; i < height; i++)
	{
		downSampleByTwo(outputImg, heightImg);
		computeError(errorImg, originImg, outputImg);

		sprintf(name, "%s/%d_output.png", outputFolder, i);
		savePNG(outputImg, name);

		sprintf(name, "%s/%d_error.png", outputFolder, i);
		savePNG(errorImg, name);

		heightImg = outputImg;
	}

	return 0;
}
