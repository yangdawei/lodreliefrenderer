%% computeErrorPerFace: Compute downsampling error per face(triangle)
function [errorPerFace, invMap] = computeErrorPerFace(imgData, wobj, treeHeight)
    % Read triangle and texture coordinates
    triangleIndices = [];
    texcoordIndices = [];

    for obj = wobj.objects
        if obj.type == 'f'
            triangleIndices = [triangleIndices; obj.data.vertices];
            texcoordIndices = [texcoordIndices; obj.data.texture];
        end
    end

    assert(size(triangleIndices, 1) == size(texcoordIndices, 1), 'Triangle & Texture coordinates indices mismatch!');
    assert(size(triangleIndices, 2) == 3 && size(texcoordIndices, 2) == 3, 'Not triangle faces!');

    % Read vertices and textures;
    vertices = wobj.vertices;
    textures = wobj.vertices_texture;
    assert(size(vertices, 2) == 3, 'Not 3-d vertices!');

    if size(textures, 2) ~= 2
        warning('Not 2-d texture coordinates!');
        textures = textures(:, 1:2);
    end


    shapeInserter = vision.ShapeInserter('Shape', 'Polygons', 'Fill', false, 'BorderColor', 'Custom', 'CustomBorderColor', [1, 0, 0]);

    texcoord = textures(reshape(texcoordIndices', 1, numel(texcoordIndices)), :);
    texcoord = texcoord .* repmat(size(imgData), size(texcoord, 1), 1);
    t_flat = reshape(texcoord', 1, numel(texcoord));
    invMap = step(shapeInserter, repmat(imgData, [1, 1, 3]), t_flat);

    shapeInserter = vision.ShapeInserter('Shape', 'Polygons', 'Fill', true, 'BorderColor', 'White', 'FillColor', 'White');
    blank = zeros(size(imgData));

    errorPerFace = zeros(size(triangleIndices, 1), treeHeight - 1);

    for hidx = 1:treeHeight - 1

        dim = size(imgData) ./ 2 ^ (treeHeight - hidx);
        [~, currentError] = downsampleWithError(imgData, dim);
        currentErrorSquare = currentError .* currentError;

        for tidx = 1:size(triangleIndices, 1)
            fprintf('%d/%d\n', tidx, size(triangleIndices, 1));
            texcoord = textures(texcoordIndices(tidx, :), :);
            texcoord = texcoord .* repmat(size(imgData), size(texcoord, 1), 1);
            t_flat = reshape(texcoord', 1, numel(texcoord));
            painted = step(shapeInserter, blank, t_flat);
            mask = (painted ~= 0);

            errorPerFace(tidx, hidx) = sqrt(sum(sum(mask .* currentErrorSquare)) / sum(mask(:)));
            disp(errorPerFace(tidx, hidx));
        end
    end
end
