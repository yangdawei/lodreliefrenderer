function buildPerfectQuadtree(imgFile, objFile, treeHeight, outputDir)

    % Read grey-scale image data as height map
    % According to treeHeight build a perfect quadratic tree
    % Save data to {num}.data and errors per triangle to info.txt

    imgData = imread(imgFile);
    len = length(size(imgData));
    if len == 3
        hData = im2double(imgData(:, :, 1));
    else
        hData = im2double(imgData);
    end
    n = 2 ^ (treeHeight - 1);
    [originWidth, originHeight] = size(hData);

    assert(originWidth > n && originHeight > n, 'Tree height too large!');

    width = originWidth / n;
    height = originHeight / n;

    totalNum = (4 ^ treeHeight - 1) / 3;

    % Hierarchy traversal
    import java.util.LinkedList;
    nodeQueue = LinkedList();

    % idx, xmin, ymin, xmax, ymax
    nodeQueue.add({0, 0, 0, originWidth, originHeight});

    idx = 1;
    while idx < totalNum
        front = cell(nodeQueue.remove());
        [cidx, xmin, ymin, xmax, ymax] = front{:};
        xmid = (xmin + xmax) / 2;
        ymid = (ymin + ymax) / 2;


        nodeQueue.add({idx+0, xmin, ymin, xmid, ymid});
        nodeQueue.add({idx+1, xmid, ymin, xmax, ymid});
        nodeQueue.add({idx+2, xmin, ymid, xmid, ymax});
        nodeQueue.add({idx+3, xmid, ymid, xmax, ymax});

        fprintf('Node %d: (%d %d %d %d)\n', cidx, xmin, ymin, xmax, ymax);
        img = imresize(hData(xmin+1:xmax, ymin+1:ymax), [width, height]);
        imwrite(img, sprintf('%s/%d.png', outputDir, cidx));
        saveRaw(img, sprintf('%s/%d.dat', outputDir, cidx));

        idx = idx + 4;
    end

    while ~nodeQueue.isEmpty()
        front = cell(nodeQueue.remove());
        [cidx, xmin, ymin, xmax, ymax] = front{:};

        fprintf('Node %d: (%d %d %d %d)\n', cidx, xmin, ymin, xmax, ymax);
        img = imresize(hData(xmin+1:xmax, ymin+1:ymax), [width, height]);
        saveRaw(img, sprintf('%s/%d.dat', outputDir, cidx));
        imwrite(img, sprintf('%s/%d.png', outputDir, cidx));
    end

    errorsPerFace = computeErrorPerFace(hData, read_wobj(objFile), treeHeight);

    fid = fopen([outputDir '/info.txt'], 'w');

    fprintf(fid, '%d %d\n', originWidth, originHeight);
    fprintf(fid, '%d\n', treeHeight);
    fprintf(fid, '\n');
    fprintf(fid, [repmat('%.10e ', 1, size(errorsPerFace, 2)), '\n'], errorsPerFace');

    fclose(fid);

end
