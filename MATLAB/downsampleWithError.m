%% downsampleWithError: downsample image and return max error
function [output, err] = downsampleWithError(img, dim)
    odim = size(img);
    output = imresize(img, dim);

    resized = imresize(output, odim);
    % err = sqrt(sum(sum((resized - img) .^ 2)) / prod(odim));
    err = resized - img;
