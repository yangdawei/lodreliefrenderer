#include "ImageHelper.h"
#include "lodepng/lodepng.h"
#include "glm/glm.hpp"

using namespace std;

void savePNG(const Image1f &grey, const char *filename)
{
	assert(grey.height() > 0);
	assert(grey.width() > 0);

	vector<unsigned char> imageData;
	imageData.reserve(grey.height() * grey.width() * 4);
	for (int y = 0; y < grey.height(); y++)
	{
		for (int x = 0; x < grey.width(); x++)
		{
			for (int k = 0; k < 3; k++)
				imageData.push_back(grey.get(x, y) * 255);
			imageData.push_back(255);
		}
	}

	unsigned error = lodepng::encode(filename, imageData, grey.width(), grey.height());
	if (error)
	{
		printf("Encode png file %s error: %s\n", filename, lodepng_error_text(error));
		assert(false);
	}
}

void loadColorPNG(Image<glm::vec3> &colorImage, const char *filename)
{
	vector<unsigned char> imageData;
	unsigned int w, h;
	unsigned error = lodepng::decode(imageData, w, h, filename);
	if (error)
	{
		printf("Decode png file %s error: %s\n", filename, lodepng_error_text(error));
		assert(false);
	}
	int strip = imageData.size() / (w * h);
	assert(strip == 3 || strip == 4);

	colorImage.resize(w, h);
	int idx = 0;
	for (int i = 0; i < int(h); i++)
	{
		for (int j = 0; j < int(w); j++)
		{
			colorImage.set(j, i, glm::vec3(imageData[idx] / 255.0, imageData[idx + 1] / 255.0, imageData[idx + 2] / 255.0));
			idx += strip;
		}
	}
}

void saveColorPNG(const Image<glm::vec3> &colorImage, const char *filename)
{
	vector<unsigned char> imageData;
	unsigned int w = colorImage.width(), h = colorImage.height();
	for (int i = h - 1; i >= 0; i--)
	{
		for (int j = 0; j < int(w); j++)
		{
			glm::vec3 tmp = colorImage.get(j, i);
			imageData.push_back(tmp[0] * 255);
			imageData.push_back(tmp[1] * 255);
			imageData.push_back(tmp[2] * 255);
		}
	}
	unsigned error = lodepng::encode(filename, imageData, w, h, LCT_RGB);
	if (error)
	{
		printf("Encode png file %s error: %s\n", filename, lodepng_error_text(error));
		assert(false);
	}
}
