#include "Image.hpp"
#include "glm/glm.hpp"

void savePNG(const Image1f &greyImage, const char *filename);
void loadColorPNG(Image<glm::vec3> &colorImage, const char *filename);
void saveColorPNG(const Image<glm::vec3> &colorImage, const char *filename);
