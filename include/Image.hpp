#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <string>
#include <cassert>
#include <vector>
#include <cstdio>

template <typename T>
class Image
{
protected:
	std::vector<T> imageData;
	unsigned w, h;
public:
	const void* getData() const { return imageData.data(); }
	void *getData() { return imageData.data(); }
	Image(){}
	Image(unsigned w, unsigned h) { resize(w, h); }

	void resize(unsigned w, unsigned h)
	{
		this->w = w;
		this->h = h;
		imageData.resize(w*h);
	}

	unsigned width() const { return w; }
	unsigned height() const { return h; }

	T &get(unsigned x, unsigned y)
	{
		if (!(x<w && y<h))
		{
			assert(false);
			return imageData[(y*w + x) % (w * h)];
		}
		return imageData[y*w + x];
	}
	T get(unsigned x, unsigned y) const
	{
		if (!(x<w && y<h))
		{
			assert(false);
			return imageData[(y*w + x) % (w * h)];
		}
		return imageData[y*w + x];
	}
	void set(unsigned x, unsigned y, T val)
	{
		assert(x < w && y < h);
		imageData[y*w + x] = val;
	}

	void saveRaw(const std::string &filename)
	{
		FILE *fp = fopen(filename.c_str(), "wb");
		fwrite(imageData.data(), sizeof(T), imageData.size(), fp);
		fclose(fp);
	}
	void readRaw(const std::string &filename)
	{
		FILE *fp = fopen(filename.c_str(), "rb");
		fread(imageData.data(), sizeof(T), imageData.size(), fp);
		fclose(fp);
	}

	void stretch(unsigned int newWidth, unsigned int newHeight)
	{
		if (newWidth == width() && newHeight == height())
			return;

		Image<T> originImage = *this;
		unsigned int w = width();
		unsigned int h = height();
		resize(newWidth, newHeight);
		/*
		for (size_t x = 0; x < newWidth; x++) for (size_t y = 0; y < newHeight; y++)
			set(x, y, 0.1);
			*/

		for (unsigned int x = 0; x < newWidth - 1; x++)
		{
			for (unsigned int y = 0; y < newHeight - 1; y++)
			{
				float u = x * (w - 1) / float(newWidth - 1);
				float v = y * (h - 1) / float(newHeight - 1);
				int u_i = int(u);
				float u_f = u - u_i;
				int v_i = int(v);
				float v_f = v - v_i;

				T &t1 = originImage.get(u_i, v_i);
				T &t2 = originImage.get(u_i + 1, v_i);
				T &t3 = originImage.get(u_i, v_i + 1);
				T &t4 = originImage.get(u_i + 1, v_i + 1);

				set(x, y, T(0)
					+ t1 * (1 - u_f) * (1 - v_f)
					+ t2 * u_f * (1 - v_f)
					+ t3 * (1 - u_f) * v_f
					+ t4 * u_f * v_f);
			}
		}
		// Last row
		for (unsigned int x = 0; x < newWidth - 1; x++)
		{
			float u = x * (w - 1) / float(newWidth - 1);
			int u_i = int(u);
			float u_f = u - u_i;

			T &t1 = originImage.get(u_i, h - 1);
			T &t2 = originImage.get(u_i + 1, h - 1);

			set(x, newHeight - 1, T(0)
				+ t1 * (1 - u_f)
				+ t2 * u_f);
		}
		// Last column
		for (unsigned int y = 0; y < newHeight - 1; y++)
		{
			float v = y * (h - 1) / float(newHeight - 1);
			int v_i = int(v);
			float v_f = v - v_i;

			T &t1 = originImage.get(w - 1, v_i);
			T &t3 = originImage.get(w - 1, v_i + 1);

			set(newWidth - 1, y, T(0)
				+ t1 * (1 - v_f)
				+ t3 * v_f);
		}
		// Last element
		set(newWidth - 1, newHeight - 1, originImage.get(w - 1, h - 1));
	}
};

typedef Image<float> Image1f;

#endif
