#ifndef FPSCOUNTER_HPP
#define FPSCOUNTER_HPP

#include <chrono>

class FPSCounter
{
public:
	FPSCounter(float updateInterval = 100.f)
		: m_updateInterval(updateInterval)
	{
		clear();
		lastTime = std::chrono::high_resolution_clock::now();
	}

	float count()
	{
		auto now = std::chrono::high_resolution_clock::now();
		auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(now - lastTime)
			.count();
		if (diff > m_updateInterval)
		{
			lastFPS = 1000.f * m_counts / (float) diff;
			m_counts = 0;
			lastTime = now;
		}
		else
			m_counts++;

		return lastFPS;
	}

	void clear()
	{
		lastTime = std::chrono::high_resolution_clock::now();
	}

private:
	unsigned long m_counts;
	const float m_updateInterval;

	float lastFPS;
	std::chrono::time_point<std::chrono::system_clock> lastTime;
};

#endif