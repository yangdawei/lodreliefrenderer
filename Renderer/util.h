#ifndef UTIL_H
#define UTIL_H

#ifndef NDEBUG
#define _CheckErrorGL  \
{ \
	GLenum error = glGetError(); \
	if (error != GL_NO_ERROR) \
			{ \
		const GLubyte *descrption = gluErrorString(error); \
		std::cerr << error << descrption << std::endl; \
		assert(false); \
			} \
}
#else
#define _CheckErrorGL (void)0
#endif

#endif
