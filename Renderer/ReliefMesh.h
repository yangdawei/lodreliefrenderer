#ifndef RELIEF_MESH_H
#define RELIEF_MESH_H

#include "../include/GLUT/freeglut.h"
#include "../include/glm/glm.hpp"
#include "LODTree.h"

#include <vector>

class ReliefMesh
{
public:
	explicit ReliefMesh(std::string modelFile, std::string texFolder, std::string texPngFile)
	{ 
		loadModel(modelFile); 
		loadTex(texFolder, texPngFile);
		computeFaceGrids();
	}

	void init();
	void release();
	void loadModel(std::string modelFile);
	void loadObj(std::string modelFile);
	void loadPly(std::string modelFile);
	void loadTex(std::string texFolder, std::string texPngFile);
	void computeFaceGrids();
	void draw(bool compute = false) const;
	GLint getTreeHeight() { return treeHeight; }
	glm::mat4 &transMat() { return m_transMat; }
	glm::mat4 &rotMat() { return m_rotMat; }
	glm::mat4 modelMat() { return m_transMat * m_rotMat; }

	// GLuint id() { return objectID; }

private:
	void autogenNormals();
	void unitize();

	// Vertex attributes
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::uvec3> indices;

	// Face attributes
	std::vector<glm::vec2> texcoords;
	std::vector<glm::vec3> faceNormals;
	std::vector<glm::uvec3> faceTexcoords;
	std::vector<GLfloat> faceErrors;

	std::vector<std::vector<std::set<glm::uint>>> faceIntersections;

	enum vao_ids { TRIANGLES, NUM_VAOS };
	enum buffer_ids { ARRAY_VERTEX, ARRAY_NORMAL, ARRAY_TEXCOORD, ARRAY_FACEERROR_0, ARRAY_FACEERROR_1, LEVEL_FEEDBACK, NUM_BUFFERS };
	enum attrib_ids { V_POSITION, N_POSITION, T_POSITION, E0_POSITION, E1_POSITION };
	enum texture_ids {
		HEIGHT_MAP_LOD,			// image array of height maps in GPU
		TREE_TEX_ARR_INDEX,		// Final stored texture index of each face
		FEEDBACK_ARRAY,			// Level-computing feedback
		COLOR_TEXTURE,			// The surface color texture.
		NUM_TEXTURES
	};

	// GLuint objectID;

	GLint originWidth, originHeight, treeHeight;
	LODTree lodtree;

	GLuint vaos[NUM_VAOS];
	GLuint buffers[NUM_BUFFERS];
	GLuint textures[NUM_TEXTURES];

	size_t array_size;
	size_t indices_size;
	float sphereRadius;
	glm::vec3 sphereCenter;

	glm::mat4 m_transMat = glm::mat4();
	glm::mat4 m_rotMat = glm::mat4();
};

#endif
