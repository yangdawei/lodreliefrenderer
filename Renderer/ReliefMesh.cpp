#define _CRT_SECURE_NO_WARNINGS
// #define FLOAT_FEEDBACK

#include <fstream>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <io.h>
#include <direct.h>

#include "../include/GLEW/glew.h"
#include "../include/rply/rply.h"
#include "ReliefMesh.h"
#include "Renderer.h"
#include "util.h"

using namespace std;
using namespace glm;

void ReliefMesh::init()
{
	GLsizei maxTextureSize;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTextureSize);

	// Vertex attributes
	glGenVertexArrays(NUM_VAOS, vaos);
	glBindVertexArray(vaos[TRIANGLES]);

	array_size = indices.size() * 3;
	indices_size = indices.size();

	GLfloat *verts = new GLfloat[3 * array_size];
	GLfloat *norms = new GLfloat[4 * array_size];
	GLfloat *coord = new GLfloat[2 * array_size];
	GLfloat *err_0 = new GLfloat[4 * array_size];
	GLfloat *err_1 = new GLfloat[4 * array_size];

	assert(array_size == faceTexcoords.size() * 3);
	assert(array_size == faceNormals.size() * 3);

	for (size_t i = 0; i < indices.size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			for (int k = 0; k < 3; k++)
			{
				verts[3 * (3 * i + j) + k] = vertices[indices[i][j]][k];
				norms[4 * (3 * i + j) + k] = faceNormals[i][k];
			}
			for (int k = 0; k < 2; k++)
			{
				coord[2 * (3 * i + j) + k] = texcoords[faceTexcoords[i][j]][k];
			}
			// TODO:
			norms[4 * (3 * i + j) + 3] = 0.251f;

			if (treeHeight - 1 <= 4)
			{
				for (int k = 0; k < treeHeight - 1; k++)
					err_0[4 * (3 * i + j) + k] = faceErrors[(treeHeight - 1) * i + k];
			}
			else
			{
				for (int k = 0; k < 4; k++)
					err_0[4 * (3 * i + j) + k] = faceErrors[(treeHeight - 1) * i + k];
				for (int k = 4; k < treeHeight - 1; k++)
					err_1[4 * (3 * i + j) + (k - 4)] = faceErrors[(treeHeight - 1) * i + k];
			}
		}
	}

	// Buffers
	glGenBuffers(NUM_BUFFERS, buffers);

	// Vertices
	glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * array_size * sizeof(GLfloat),
		verts, GL_STATIC_READ);
	glVertexAttribPointer(V_POSITION, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(V_POSITION);

	// Normals (per face)
	glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 4 * array_size * sizeof(GLfloat),
		norms, GL_STATIC_READ);
	glVertexAttribPointer(N_POSITION, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(N_POSITION);

	// Coords (per vertex)
	glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_TEXCOORD]);
	glBufferData(GL_ARRAY_BUFFER, 2 * array_size * sizeof(GLfloat),
		coord, GL_STATIC_READ);
	glVertexAttribPointer(T_POSITION, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(T_POSITION);

	// Errors (per face)
	glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_FACEERROR_0]);
	glBufferData(GL_ARRAY_BUFFER, 4 * array_size * sizeof(GLfloat),
		err_0, GL_STATIC_READ);
	glVertexAttribPointer(E0_POSITION, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(E0_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, buffers[ARRAY_FACEERROR_1]);
	glBufferData(GL_ARRAY_BUFFER, 4 * array_size * sizeof(GLfloat),
		err_1, GL_STATIC_READ);
	glVertexAttribPointer(E1_POSITION, 4, GL_FLOAT, GL_FALSE, 0, (void *)0);
	glEnableVertexAttribArray(E1_POSITION);


	delete[] verts;
	delete[] norms;
	delete[] coord;
	delete[] err_0;
	delete[] err_1;

	indices.clear();
	vertices.clear();
	normals.clear();
	faceNormals.clear();
	faceErrors.clear();

	// Textures
	glGenTextures(NUM_TEXTURES, textures);

	// Height map LOD 2d texture array
	// TODO:
	_CheckErrorGL;
	glActiveTexture(GL_TEXTURE0 + HEIGHT_MAP_LOD);
	glBindTexture(GL_TEXTURE_2D_ARRAY, textures[HEIGHT_MAP_LOD]);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_R32F, lodtree.width(), lodtree.height(), lodtree.depth(), 0,
		GL_RED, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	_CheckErrorGL;
	glActiveTexture(GL_TEXTURE0 + TREE_TEX_ARR_INDEX);
	glBindTexture(GL_TEXTURE_1D, textures[TREE_TEX_ARR_INDEX]);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_R32UI, (GLsizei)lodtree.length(), 0,
		GL_RED_INTEGER, GL_UNSIGNED_INT, NULL);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	_CheckErrorGL;
	glActiveTexture(GL_TEXTURE0 + FEEDBACK_ARRAY);
	glBindTexture(GL_TEXTURE_1D, textures[FEEDBACK_ARRAY]);
	glTexImage1D(GL_TEXTURE_1D, 0, GL_R32UI, (GLsizei)indices_size, 0,
		GL_RED_INTEGER, GL_UNSIGNED_INT, NULL);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);


	// Color texture
	_CheckErrorGL;
	glActiveTexture(GL_TEXTURE0 + COLOR_TEXTURE);
	glBindTexture(GL_TEXTURE_2D, textures[COLOR_TEXTURE]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, (GLsizei)lodtree.colorTexWidth(), (GLsizei)lodtree.colorTexHeight(), 0,
		GL_RGB, GL_FLOAT, lodtree.textureData());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Transform feedback buffer
	const GLchar *feedbackVaryings[] = { "level" };
	glTransformFeedbackVaryings(Renderer::instance().computeProgram(), 1,
		feedbackVaryings, GL_INTERLEAVED_ATTRIBS);
	glBindBuffer(GL_ARRAY_BUFFER, buffers[LEVEL_FEEDBACK]);
	glBufferData(GL_ARRAY_BUFFER, indices_size * sizeof(GLfloat),
		nullptr, GL_STATIC_READ);

	_CheckErrorGL;

	glActiveTexture(GL_TEXTURE0 + HEIGHT_MAP_LOD);
	glBindTexture(GL_TEXTURE_2D_ARRAY, textures[HEIGHT_MAP_LOD]);
	lodtree.init();
	_CheckErrorGL;
}

void ReliefMesh::release()
{
	glDeleteVertexArrays(NUM_VAOS, vaos);
	glDeleteBuffers(NUM_BUFFERS, buffers);
	glDeleteTextures(NUM_TEXTURES, textures);
}

void ReliefMesh::draw(bool compute /*=false*/) const
{
	glBindVertexArray(vaos[TRIANGLES]);
	if (compute)
	{
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, buffers[LEVEL_FEEDBACK]);
		glEnable(GL_RASTERIZER_DISCARD);
		glBeginTransformFeedback(GL_POINTS);
	}
	else
	{
		glActiveTexture(GL_TEXTURE0 + HEIGHT_MAP_LOD);
		glBindTexture(GL_TEXTURE_2D_ARRAY, textures[HEIGHT_MAP_LOD]);
		glActiveTexture(GL_TEXTURE0 + TREE_TEX_ARR_INDEX);
		glBindTexture(GL_TEXTURE_1D, textures[TREE_TEX_ARR_INDEX]);
		glActiveTexture(GL_TEXTURE0 + COLOR_TEXTURE);
		glBindTexture(GL_TEXTURE_2D, textures[COLOR_TEXTURE]);
		glActiveTexture(GL_TEXTURE0 + FEEDBACK_ARRAY);
		glBindTexture(GL_TEXTURE_1D, textures[FEEDBACK_ARRAY]);
	}

	glDrawArrays(GL_TRIANGLES, 0, GLsizei(array_size));
	
	// If debug is not necessary, this section can be optimized
	if (compute)
	{
		glEndTransformFeedback();
		glFlush();

		static vector<GLuint> feedback;
		feedback.resize(indices_size);
		glGetBufferSubData(GL_TRANSFORM_FEEDBACK_BUFFER	, 0,
			indices_size * sizeof(GLuint), feedback.data());

		glDisable(GL_RASTERIZER_DISCARD);

		puts("feedback: ");
		// printf("%u\n", feedback[0]);
		for (size_t i = 0; i < 10; i++)
			printf("%u ", feedback[i]);
		puts("\n");

		set<uint> updates;
		if (Renderer::currentStatus() != Renderer::SHOW_ERROR)
		{
			for (int i = 0; i < indices_size; i++)
			{
				const auto &ref = faceIntersections[feedback[i]][i];
				updates.insert(ref.begin(), ref.end());
			}
		}

		LODTree &tree = const_cast<LODTree &>(lodtree);

		// TODO

		tree.update(updates);

		glActiveTexture(GL_TEXTURE0 + FEEDBACK_ARRAY);
		glBindTexture(GL_TEXTURE_1D, textures[FEEDBACK_ARRAY]);
		glTexImage1D(GL_TEXTURE_1D, 0, GL_R32UI, (GLsizei)feedback.size(), 0, GL_RED_INTEGER, GL_UNSIGNED_INT, feedback.data());
		_CheckErrorGL;


		glActiveTexture(GL_TEXTURE0 + TREE_TEX_ARR_INDEX);
		glBindTexture(GL_TEXTURE_1D, textures[TREE_TEX_ARR_INDEX]);
		glTexImage1D(GL_TEXTURE_1D, 0, GL_R32UI, (GLsizei)lodtree.length(), 0,
			GL_RED_INTEGER, GL_UNSIGNED_INT, lodtree.arrIndices());

		glActiveTexture(GL_TEXTURE0 + HEIGHT_MAP_LOD);
		glBindTexture(GL_TEXTURE_2D_ARRAY, textures[HEIGHT_MAP_LOD]);
		const auto &modSet = lodtree.getModifiedSet();
		for (auto iter = modSet.begin(); iter != modSet.end(); iter++)
		{
			glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, GLint(*iter), lodtree.width(), lodtree.height(), 1,
				GL_RED, GL_FLOAT, lodtree.heightMapAt(lodtree.inverseIndices()[*iter]));
		}
	}
}

void ReliefMesh::autogenNormals()
{
	normals = vertices;
	faceNormals.clear();
	vector<vector<vec3>> normOfVertices(vertices.size());
	for (size_t i = 0; i < indices.size(); i++)
	{
		GLuint u1 = indices[i][0];
		vec3 p1 = vertices[u1];
		GLuint u2 = indices[i][1];
		vec3 p2 = vertices[u2];
		GLuint u3 = indices[i][2];
		vec3 p3 = vertices[u3];
		vec3 faceNorm = glm::normalize(cross(p2 - p1, p3 - p1));
		normOfVertices[indices[i][0]].push_back(faceNorm);
		normOfVertices[indices[i][1]].push_back(faceNorm);
		normOfVertices[indices[i][2]].push_back(faceNorm);
		faceNormals.push_back(faceNorm);
	}

	for (size_t i = 0; i < normals.size(); i++)
	{
		vec3 sum = vec3(0, 0, 0);
		for (size_t j = 0; j < normOfVertices[i].size(); j++)
			sum += normOfVertices[i][j];
		normals[i] = normalize(sum);
	}
}

void ReliefMesh::unitize()
{
	// Compute approximate minimal bounding sphere
	auto length = vertices.size();
	assert(length > 1);
	auto half = length / 2;

	vec3 p0 = vertices[0];
	vec3 q0 = vertices[half];
	sphereCenter = (p0 + q0) / 2.f;
	sphereRadius = glm::distance(p0, q0) / 2;

	for (size_t i = 0; i < vertices.size(); i++)
	{
		vec3 s = vertices[i];
		vec3 diff = sphereCenter - s;
		if (glm::length(diff) > sphereRadius)
		{
			diff = normalize(diff);
			vec3 t = diff * sphereRadius + sphereCenter;
			sphereCenter = (s + t) / 2.f;
			sphereRadius = glm::distance(s, t) / 2.f;
		}
	}

	for (size_t i = 0; i < vertices.size(); i++)
	{
		vertices[i] = (vertices[i] - sphereCenter) / sphereRadius;
	}
}

void ReliefMesh::loadObj(string modelFile)
{
	ifstream input(modelFile);
	if (input.fail())
	{
		cerr << "Error opening " + modelFile + ": " << strerror(errno);
		assert(false);
		exit(0);
	}

	string header;
	string tmps;
	float x, y, z;
	for (string line; getline(input, line);)
	{
		if (line.size() > 0)
		{
			istringstream is(line);
			is >> header;
			if (header == "v")
			{
				is >> x >> y >> z;
				vertices.push_back(glm::vec3(x, y, z));
			}
			else if (header == "vn")
			{
			}
			else if (header == "vt")
			{
				is >> x >> y;
				texcoords.push_back(glm::vec2(x, y));
			}
			else if (header == "f")
			{
				glm::uvec3 tmpvi;
				glm::uvec3 tmpti;
				for (int i = 0; i < 3; i++)
				{
					is >> tmps;
					auto p1 = tmps.find_first_of('/');
					istringstream iv(tmps.substr(0, p1));
					iv >> tmpvi[i];
					istringstream in(tmps.substr(p1 + 1));
					in >> tmpti[i];
				}
				indices.push_back(tmpvi - glm::uvec3(1));
				faceTexcoords.push_back(uvec3(tmpti) - uvec3(1, 1, 1));
			}
			else
			{
			}
		}
	}
}

template <typename T>
void vector_pushback_callback(p_ply_argument argument)
{
	vector<T> *pVec;
	ply_get_argument_user_data(argument, (void **)&pVec, NULL);
	double result = ply_get_argument_value(argument);
	pVec->push_back((T)result);
}

void ReliefMesh::loadPly(string modelFile)
{
	// Callback functions (lambda expression)
	auto vertex_cb = [](p_ply_argument argument)
	{
		vector_pushback_callback<GLfloat>(argument);
		return 1;
	};
	auto triangle_cb = [](p_ply_argument argument)
	{
		long length, value_index;
		ply_get_argument_property(argument, NULL, &length, &value_index);
		assert(length == 3);

		if (value_index >= 0)
			vector_pushback_callback<GLuint>(argument);

		return 1;
	};
	auto error_cb = [](p_ply ply, const char *message)
	{
		char *buffer;
		ply_get_ply_user_data(ply, (void **)&buffer, NULL);
		printf("Error parsing ply file: %s\n", buffer);
	};

	long nvertices, ntriangles;
	p_ply ply = ply_open(modelFile.c_str(), error_cb, 0, (void *)modelFile.c_str());
	assert(ply);
	int result = ply_read_header(ply);
	assert(result);

	nvertices = ply_set_read_cb(ply, "vertex", "x", vertex_cb, &vertices, 0);
	ply_set_read_cb(ply, "vertex", "y", vertex_cb, &vertices, 1);
	ply_set_read_cb(ply, "vertex", "z", vertex_cb, &vertices, 2);
	vertices.reserve(nvertices * 3);

	ntriangles = ply_set_read_cb(ply, "face", "vertex_indices", triangle_cb, &indices, 0);
	indices.reserve(ntriangles * 3);

	result = ply_read(ply);
	assert(result);
	ply_close(ply);

	//TODO: correct texcoords
	texcoords.push_back(vec2(0, 0.1));
	texcoords.push_back(vec2(0.1, 0));
	texcoords.push_back(vec2(0.1, 0.1));

	faceTexcoords = vector<glm::uvec3>(ntriangles, uvec3(0, 1, 2));
}

void ReliefMesh::loadModel(string modelFile)
{
	vertices.clear();
	indices.clear();
	texcoords.clear();
	faceTexcoords.clear();

	assert(modelFile.size() >= 4);
	string::size_type sz = modelFile.size();
	if (modelFile.substr(sz - 4) == ".ply")
		loadPly(modelFile);
	else
		loadObj(modelFile);
	indices_size = indices.size();

	auto normalCached = [](string modelFileName)
	{
		std::replace(modelFileName.begin(), modelFileName.end(), '/', '_');
		char buffer[4096];
		sprintf(buffer, ".cache/%s.norm", modelFileName.c_str());
		return (_access(buffer, 0) == 0);
	};
	auto loadCachedNormal = [](string modelFileName, vector<glm::vec3> &faceNormals)
	{
		std::replace(modelFileName.begin(), modelFileName.end(), '/', '_');
		char buffer[4096];
		sprintf(buffer, ".cache/%s.norm", modelFileName.c_str());
		size_t length;
		FILE *fp = fopen(buffer, "rb");
		fread(&length, sizeof(size_t), 1, fp);
		faceNormals.resize(length);
		fread(faceNormals.data(), sizeof(glm::vec3), length, fp);
		fclose(fp);
	};

	auto saveNormalCache = [](string modelFileName, vector<glm::vec3> faceNormals)
	{
		if (_access(".cache", 0) != 0)
			_mkdir(".cache");

		std::replace(modelFileName.begin(), modelFileName.end(), '/', '_');
		char buffer[4096];
		sprintf(buffer, ".cache/%s.norm", modelFileName.c_str());
		size_t length = faceNormals.size();
		FILE *fp = fopen(buffer, "wb");
		fwrite(&length, sizeof(size_t), 1, fp);
		fwrite(faceNormals.data(), sizeof(glm::vec3), length, fp);
		fclose(fp);
	};

#ifdef CACHED_NORMAL
	if (normalCached(modelFile))
		loadCachedNormal(modelFile, faceNormals);
	else
	{
		autogenNormals();
		saveNormalCache(modelFile, faceNormals);
	}
#else
	autogenNormals();
#endif
}

void ReliefMesh::loadTex(string texFolder, string texPngFile)
{
	// Read info: original texture width and height
	// The tree height;
	FILE *fp = fopen((texFolder + "/info.txt").c_str(), "r");
	fscanf(fp, "%d %d", &originWidth, &originHeight);
	fscanf(fp, "%d", &treeHeight);

	// Read error per face
	float tmpf;
	faceErrors.clear();
	faceErrors.reserve(indices_size * (treeHeight - 1));
	for (int i = 0; i < indices_size * (treeHeight - 1); i++)
	{
		fscanf(fp, "%f", &tmpf);
		faceErrors.push_back(tmpf);
	}

	fclose(fp);
	int ratio = 1 << (treeHeight - 1);

	lodtree.load(texFolder, texPngFile, treeHeight, originWidth / ratio, originHeight / ratio);
}

void ReliefMesh::computeFaceGrids()
{
	assert(indices.size() > 0);
	assert(treeHeight > 0);
	assert(lodtree.width() > 0 && lodtree.height() > 0);

	faceIntersections.resize(treeHeight);
	for (int i = 0; i < treeHeight; i++)
		faceIntersections[i].resize(indices_size);

	const int ampl = 1 << (treeHeight - 1);
	vector<set<uint>> &faceIntsDeepest = faceIntersections[treeHeight - 1];

	auto pointInRectangle = [](vec2 p, vec2 rec0, vec2 rec1) -> bool
	{
		if (p.s >= rec0.s && p.s <= rec1.s && p.t >= rec0.t && p.t <= rec1.t)
			return true;
		return false;
	};
	auto pointInTriangle = [](vec2 p, vec2 tri0, vec2 tri1, vec2 tri2) -> bool
	{
		vec2 v0 = tri1 - tri0;
		vec2 v1 = tri2 - tri0;
		vec2 v2 = p - tri0;
		float dot00 = glm::dot(v0, v0);
		float dot01 = glm::dot(v0, v1);
		float dot02 = glm::dot(v0, v2);
		float dot11 = glm::dot(v1, v1);
		float dot12 = glm::dot(v1, v2);
		float inverDeno = 1 / (dot00 * dot11 - dot01 * dot01);
		float u = (dot11 * dot02 - dot01 * dot12) * inverDeno;
		float v = (dot11 * dot02 - dot01 * dot12) * inverDeno;
		if (u < 0 || u > 1 || v < 0 || v > 1)
			return false;
		return (u + v <= 1);
	};
	auto lineRectIntersection = [&pointInRectangle, &pointInTriangle](vec2 ln0, vec2 ln1, vec2 rec0, vec2 rec1) -> bool
	{
		if (ln0.s == ln1.s || ln0.t == ln1.t)
			return pointInRectangle(ln0, rec0, rec1) || pointInRectangle(ln1, rec0, rec1);
		
		float slope = (ln1.t - ln0.t) / (ln1.s - ln0.s);
		float t0 = (rec0.s - ln0.s) * slope + ln0.t;
		float t1 = (rec1.s - ln0.s) * slope + ln0.t;
		float s0 = (rec0.t - ln0.t) / slope + ln0.s;
		float s1 = (rec1.t - ln0.t) / slope + ln0.s;

		return (rec0.s <= s0 && s0 <= rec1.s)
			|| (rec0.s <= s1 && s1 <= rec1.s)
			|| (rec0.t <= t0 && t0 <= rec1.t)
			|| (rec0.t <= t1 && t1 <= rec1.t);
	};
	auto intersectionTest = [&pointInTriangle, pointInRectangle, lineRectIntersection](vec2 tri0, vec2 tri1, vec2 tri2, vec2 rec0, vec2 rec1) -> bool
	{
		if (pointInRectangle(tri0, rec0, rec1) || pointInRectangle(tri1, rec0, rec1) || pointInRectangle(tri2, rec0, rec1))
			return true;
		if (pointInTriangle(rec0, tri0, tri1, tri2) || pointInTriangle(rec1, tri0, tri1, tri2)
			|| pointInTriangle(vec2(rec0.s, rec1.t), tri0, tri1, tri2) || pointInTriangle(vec2(rec1.s, rec0.t), tri0, tri1, tri2))
			return true;
		if (lineRectIntersection(tri0, tri1, rec0, rec1) || lineRectIntersection(tri1, tri2, rec0, rec1) || lineRectIntersection(tri2, tri0, rec0, rec1))
			return true;

		return false;
	};

	for (size_t i = 0; i < indices.size(); i++)
	{
		int minS, minT, maxS, maxT;
		minS = minT = 1 << treeHeight;
		maxS = maxT = 0;
		for (int k = 0; k < 3; k++)
		{
			vec2 coord = texcoords[faceTexcoords[i][k]];
			minS = std::min(int(coord.s * ampl), minS);
			minT = std::min(int(coord.t * ampl), minT);
			maxS = std::max(int(coord.s * ampl) + 1, maxS);
			maxT = std::max(int(coord.t * ampl) + 1, maxT);
		}
		if (maxS > ampl)
			maxS = ampl;
		if (maxT > ampl)
			maxT = ampl;

		for (int s = minS; s < maxS; s++) for (int t = minT; t < maxT; t++)
		{
			if (intersectionTest(
				texcoords[faceTexcoords[i][0]] * (float) ampl,
				texcoords[faceTexcoords[i][1]] * (float) ampl,
				texcoords[faceTexcoords[i][2]] * (float) ampl,
				vec2(s, t), vec2(s + 1, t + 1)))
			{
				int sum = 0;
				for (int pos = treeHeight - 2; pos >= 0; pos--)
				{
					int bitS = (s >> pos) & 1;
					int bitT = (t >> pos) & 1;
					sum = 4 * sum + bitT * 2 + bitS + 1;
				}
				faceIntsDeepest[i].insert(sum);
			}
		}
		assert(faceIntsDeepest[i].size() > 0);
	}

	for (int level = treeHeight - 1; level >= 1; level--)
	{
		for (int i = 0; i < indices_size; i++)
		{
			for (uint idx : faceIntersections[level][i])
				faceIntersections[level - 1][i].insert((idx - 1) >> 2);
		}
	}
}
