#ifndef LODTREE_H
#define LODTREE_H

#include <vector>
#include <string>
#include <set>
#include <bitset>

#include "../include/glm/glm.hpp"
#include "../include/Image.hpp"

#include "Block.hpp"

class LODTree
{
public:
	void load(std::string folderName, std::string texPngFile, int treeHeight, int hWidth, int hHeight);

	unsigned int length() const { return (unsigned int)((1 << 2 * treeHeight) - 1) / 3 + 1; }
	void update(std::set<glm::uint> &indices);

	const void *heightMapAt(int pos) const { return heightMaps[pos]->getData(); }
	const void *textureData() const { return texture.getData(); }
	const void *arrIndices() const { return texArrIndices.data(); }
	// const void *transParams() const { return trTransParams.data(); }
	const std::vector<glm::uint> &inverseIndices() const { return invIndices; }

	// unsigned int rootWidth() const { return heightMaps[0]->width(); }
	// unsigned int rootHeight() const { return heightMaps[0]->height(); }

	unsigned int width() const { return hWidth; }
	unsigned int height() const { return hHeight; }

	unsigned int colorTexWidth() const { return texture.width(); }
	unsigned int colorTexHeight() const { return texture.height(); }
	
	// TODO: real depth
	unsigned int depth() const { return length(); }

	const std::set<glm::uint> &getModifiedSet() const{ return modifiedSet; }

	void init();

private:

	void computeUVTransformParameters();

	// std::vector<glm::vec4> trTransParams;
	// std::vector<glm::uvec4> trChildren;
	// std::vector<glm::uvec4> trSplitSize;
	// std::vector<glm::float32> trMaxError;
	std::vector<Image1f *> heightMaps;
	std::vector<glm::uint> texArrIndices;
	std::vector<glm::uint> invIndices;
	std::vector<int> dllist;	// Double-linked list

	std::set<glm::uint> curSet;
	std::set<glm::uint> modifiedSet;

	Image<glm::vec3> texture;
	Block block;


	int hWidth;
	int hHeight;
	int treeHeight;
};

#endif