#ifndef BLOCK_HPP
#define BLOCK_HPP

#include <vector>

class Block
{
public:
	Block(std::size_t size = 1U)
	{
		linkedList = std::vector<size_t>(size);
		for (size_t i = 0; i < size; i++)
		{
			linkedList[i] = i + 1;
		}
		linkedList[size - 1] = 0;
		head = 0;
	}

	void free(size_t pos)
	{
		linkedList[pos] = head;
		head = pos;
	}

	size_t alloc()
	{
		size_t ret = head;
		head = linkedList[head];
		return ret;
	}

private:

	std::vector<size_t> linkedList;
	std::size_t head;
};

#endif
