// #define RENDERER_REAL_TIME
#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <streambuf>

#include "../include/GLEW/glew.h"
#include "Renderer.h"
#include "../include/ImageHelper.h"
#include "../include/GLUT/freeglut.h"
#include "../include/rapidjson/rapidjson.h"
#include "../include/rapidjson/document.h"
#include "../include/glm/gtx/transform.hpp"
#include "../include/glm/gtc/type_ptr.hpp"
#include "loadShaders.h"
#include "util.h"

using namespace std;

Renderer::EStatus Renderer::status = Renderer::NORMAL_RENDER;

Renderer::~Renderer()
{

}

void Renderer::initFBO()
{
	// Create
	// frame buffer object
	glGenFramebuffers(1, &fboID);
	glBindFramebuffer(GL_FRAMEBUFFER, fboID);

	// depth buffer with render buffer object
	glGenRenderbuffers(1, &depth_rboID);
	glBindRenderbuffer(GL_RENDERBUFFER, depth_rboID);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, screenWidth, screenHeight);

	// color buffer with texture object
	glGenTextures(1, &color_rboID);
	glBindTexture(GL_TEXTURE_2D, color_rboID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, 0);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//Attach depth buffer to FBO
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depth_rboID);

	// Attach color buffer to FBO
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, color_rboID, 0);
	_CheckErrorGL;
}

void Renderer::init()
{
	// Initialize context
	int argc = 0;
	char argv[1][1];
	glutInit(&argc, (char **) argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_RGBA | GLUT_DOUBLE);
	glutInitContextProfile(GLUT_CORE_PROFILE);
	glutInitContextVersion(4, 2);

	// Initialize window
	glutCreateWindow("LODReliefRenderer");
	glutReshapeWindow(1280, 800);

	// Initialize GLEW
	glewExperimental = true;
	GLenum err;
	if ((err = glewInit()) != GLEW_OK)
	{
		cout << glewGetErrorString(err) << endl;
		assert(false);
	}
	// Eliminate possible GL_INVALID_ENUM error
	glGetError();

	// Initialize callbacks
	glutDisplayFunc(Renderer::displayCallback);
	glutReshapeFunc(Renderer::reshapeCallback);
	glutMouseFunc(Renderer::mouseCallback);
	glutMotionFunc(Renderer::motionCallback);
	glutIdleFunc(Renderer::idleCallback);
	glutKeyboardFunc(Renderer::keyboardCallback);

	glEnable(GL_DEPTH_TEST);
	glProvokingVertex(GL_LAST_VERTEX_CONVENTION);

	// Initialize
	// Initialize shaders
	ShaderInfo reliefInfos[] = {
			{ GL_VERTEX_SHADER, "Shader/relief.vert" },
			{ GL_GEOMETRY_SHADER, "Shader/relief.geom" },
			{ GL_FRAGMENT_SHADER, "Shader/relief.frag" },
			{ GL_NONE, NULL }
	};
	m_reliefProgram = loadShaders(reliefInfos);
	linkProgram(m_reliefProgram);

	ShaderInfo debugInfos[] = {
		{ GL_VERTEX_SHADER, "Shader/debug.vert" },
		{ GL_GEOMETRY_SHADER, "Shader/debug.geom" },
		{ GL_FRAGMENT_SHADER, "Shader/debug.frag" },
		{ GL_NONE, NULL }
	};
	m_debugProgram = loadShaders(debugInfos);
	linkProgram(m_debugProgram);


	ShaderInfo computeInfos[] = {
			{ GL_VERTEX_SHADER, "Shader/levelcompute.vert" },
			{ GL_GEOMETRY_SHADER, "Shader/levelcompute.geom" },
			{ GL_NONE, NULL }
	};
	m_computeProgram = loadShaders(computeInfos);

	// Initialize objects
	for (auto &pObj : objects)
		pObj->init();

	linkProgram(m_computeProgram);
}

void Renderer::releaseFBO()
{
	if (fboID)
	{
		glDeleteFramebuffers(1, &fboID);
		fboID = 0;
	}

	if (depth_rboID)
	{
		glDeleteRenderbuffers(1, &depth_rboID);
		depth_rboID = 0;
	}

	if (color_rboID)
	{
		glDeleteTextures(1, &color_rboID);
		color_rboID = 0;
	}
}

void Renderer::release()
{
	for (auto &pObj : objects)
	{
		pObj->release();
		delete pObj;
	}
	objects.clear();
}

void Renderer::startLoop()
{
	glutMainLoop();
}

void Renderer::display() const
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	_CheckErrorGL;

	// Render geometry
// #define DEBUG_SHADER
#ifndef DEBUG_SHADER
	glUseProgram(m_computeProgram);
	_CheckErrorGL;

	for (auto &ptr : objects)
	{
		setUniform(m_computeProgram, "status", (GLint)status);
		setUniform(m_computeProgram, "mModelView", viewMat * ptr->modelMat());
		setUniform(m_computeProgram, "mProject", projMat);
		// TODO: modify predRadius
		setUniform(m_computeProgram, "predRadius", 1.f);

		// TODO; predAccu
		setUniform(m_computeProgram, "predAccu", 2 * 1 / sin(fovy / 2.f) / screenWidth * 10.f);

		setUniform(m_computeProgram, "treeHeight", ptr->getTreeHeight());
		
		ptr->draw(true);
		_CheckErrorGL;
	}

	// Render objects
	glUseProgram(m_reliefProgram);
	for (auto &ptr : objects)
	{
		setUniform(m_reliefProgram, "status", (GLint) status);
		setUniform(m_reliefProgram, "mView", viewMat);
		setUniform(m_reliefProgram, "mModelView", viewMat * ptr->modelMat());
		setUniform(m_reliefProgram, "mProject", projMat);

		ptr->draw();
	}

	// Count FPS

#ifdef RENDERER_REAL_TIME
	float fpsVal = const_cast<FPSCounter &>(fps).count();
	static char buffer[256];
	sprintf(buffer, "LODReliefRenderer - %.2f FPS", fpsVal);
	glutSetWindowTitle(buffer);
#endif

#else

	glUseProgram(m_debugProgram);
	for (auto &ptr : objects)
	{
		setUniform(m_debugProgram, "mView", viewMat);
		setUniform(m_debugProgram, "mModelView", viewMat * ptr->modelMat());
		setUniform(m_debugProgram, "mProject", projMat);

		ptr->draw();
	}
#endif

	glutSwapBuffers();
	_CheckErrorGL;
}

void Renderer::reshape(int width, int height)
{
	projMat = glm::perspective<float>((float)M_PI_2, width / (float)height, zNear, zFar);
	glViewport(0, 0, width, height);
	screenWidth = width;
	screenHeight = height;
}
void Renderer::mouse(int button, int state, int x, int y)
{
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		arcball.setWidth(glutGet(GLUT_WINDOW_WIDTH));
		arcball.setHeight(glutGet(GLUT_WINDOW_HEIGHT));
		// arcball.saveMat(objects[0]->rotMat());
		arcball.saveMat(viewMat);
		if (state == GLUT_DOWN)
			arcball.start(x, y);
		else
			arcball.stop(x, y);
		break;

	default:
		break;
	}
}
void Renderer::motion(int x, int y)
{
	arcball.setWidth(glutGet(GLUT_WINDOW_WIDTH));
	arcball.setHeight(glutGet(GLUT_WINDOW_HEIGHT));
	viewMat = arcball.motion(x, y);
	glutPostRedisplay();
}
void Renderer::idle()
{
#ifdef RENDERER_REAL_TIME
	glutPostRedisplay();
#endif
}

void Renderer::keyboard(unsigned char key, int x, int y)
{
	static glm::vec3 direction;
	switch (key)
	{
		// Inverse matrix -> vector
	case 'w':
		direction = glm::vec3(0, 0, 1);
		break;
	case 's':
		direction = glm::vec3(0, 0, -1);
		break;
	case 'a':
		direction = glm::vec3(1, 0, 0);
		break;
	case 'd':
		direction = glm::vec3(-1, 0, 0);
		break;
	case '2':
		direction = glm::vec3(0, -1, 0);
		break;
	case 'x':
		direction = glm::vec3(0, 1, 0);
		break;
	case ' ':
		status = (EStatus)((status + 1) % N_STATUS);
		glutPostRedisplay();
		break;
	case 'p':
	{
		initFBO();

		for (status = NORMAL_RENDER; status != N_STATUS; status = (EStatus)(status+1))
		{
			glReadBuffer(GL_COLOR_ATTACHMENT0);

			display();
			glutSwapBuffers();
	
			Image<glm::vec3> image(screenWidth, screenHeight);
			glReadPixels(0, 0, screenWidth, screenHeight, GL_RGB, GL_FLOAT, image.getData());
			char buf[1024];
			sprintf(buf, "../output/%d.png", status);
			saveColorPNG(image, buf);
		}
		releaseFBO();
	}

	default:
		break;
	}
	if (key == 'w' || key == 's' || key == 'd' || key == 'a' || key == '2' || key == 'x')
	{
		const float step = 0.05f;
		viewMat = glm::translate(direction * step) * viewMat;
		glutPostRedisplay();
	}
}

glm::vec3 loadVec3(const rapidjson::Value &value)
{
	rapidjson::Value::ConstValueIterator it = value.Begin();
	return glm::vec3(it->GetDouble(), (it + 1)->GetDouble(), (it + 2)->GetDouble());
}

void Renderer::loadScene(std::string filename)
{
	// Read the file to 'str'
	ifstream fin(filename);
	if (fin.fail())
	{
		cout << strerror(errno) << endl;
		assert(false);
	}

	string str((istreambuf_iterator<char>(fin)),
		istreambuf_iterator<char>());

	// Transfer string to json object
	rapidjson::Document doc;
	bool hasError = doc.Parse(str.c_str()).HasParseError();
	assert(!hasError);

	// Shader name
	// PASS

	// Read objects mesh models

	rapidjson::Value &modelArr = doc["model"];
	if (!doc.HasMember("model") || modelArr.Empty())
	{
		cerr << "Error: Scene config file does not contain any models!"
			<< endl;
		assert(false);
		exit(0);
	}


	// View Matrix
	if (doc.HasMember("view"))
	{
		rapidjson::Value &viewParams = doc["view"];
		viewMat = glm::lookAt(
			loadVec3(viewParams["eye_pos"]),
			loadVec3(viewParams["eye_center"]),
			loadVec3(viewParams["eye_up"]));
	}

	// Projection Matrix
	if (doc.HasMember("project"))
	{
		rapidjson::Value &projParams = doc["project"];
		fovy = projParams["fovy"].GetDouble() / 180 * M_PI;
		zNear = projParams["z_near"].GetDouble();
		zFar = projParams["z_far"].GetDouble();
		projMat = glm::perspective<float>(
			fovy,
			1,
			zNear,
			zFar);
	}


	for (auto it = modelArr.Begin(); it != modelArr.End(); it++)
	{
		ReliefMesh *mesh = new ReliefMesh((*it)["file"].GetString(), (*it)["tree"].GetString(), (*it)["texture"].GetString());
		/*
		if (it->HasMember("scale"))
		{
			glm::vec3 scale;
			if ((*it)["scale"].IsArray())
				scale = glm::vec3(loadVec3((*it)["scale"]));
			else
				scale = glm::vec3((*it)["scale"].GetDouble());
			mesh->rotMat() *= glm::scale(scale);
		}
		*/

		if (it->HasMember("translate"))
			mesh->transMat() *= glm::translate(loadVec3((*it)["translate"]));

		if (it->HasMember("rotate"))
			mesh->rotMat() *= glm::rotate<float>(
				(*it)["rotate"]["angle"].GetDouble() / 180 * M_PI,
				loadVec3((*it)["rotate"]["axis"])
			);

		objects.push_back(mesh);
	}
}
