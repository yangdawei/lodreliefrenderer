#ifndef RENDERER_H
#define RENDERER_H

#define GLM_FORCE_RADIANS
#define _USE_MATH_DEFINES

#pragma comment(lib, "glew32.lib")

#include <vector>
#include <math.h>

#include "../include/glm/gtx/transform.hpp"
#include "../include/glm/gtc/type_ptr.hpp"
#include "../include/GLEW/glew.h"
#include "util.h"

#include "ReliefMesh.h"
#include "Arcball.hpp"
#include "FPSCounter.hpp"

class Renderer
{
public:

	static Renderer &instance()
	{
		static Renderer renderer;
		return renderer;
	}

	void init();
	void initFBO();
	void releaseFBO();
	void release();
	void startLoop();
	void loadScene(std::string sceneXMLfile);

	void setUniform(GLuint program, const char *name, const glm::mat4 &mat) const
	{
		GLuint location = glGetUniformLocation(program, name);
		glUniformMatrix4fv(location, 1, GL_FALSE, glm::value_ptr(mat));
		_CheckErrorGL;
	}
	void setUniform(GLuint program, const char *name, GLint value) const
	{
		GLuint location = glGetUniformLocation(program, name);
		glUniform1i(location, value);
		_CheckErrorGL;
	}
	void setUniform(GLuint program, const char *name, GLfloat value) const
	{
		GLuint location = glGetUniformLocation(program, name);
		glUniform1f(location, value);
		_CheckErrorGL;
	}
	GLuint computeProgram() { return m_computeProgram; }

	enum EStatus {
		NORMAL_RENDER, SHOW_LEVEL, SHOW_ERROR, BOUNDING_BOX, ORIGINAL, N_STATUS
	};

	static EStatus currentStatus() { return status; }

private:
	// Prevent
	Renderer() {};
	~Renderer();

	static void displayCallback()
	{ 
		instance().display();
	}
	static void reshapeCallback(int width, int height)
	{
		instance().reshape(width, height);
	}
	static void mouseCallback(int button, int state, int x, int y)
	{
		instance().mouse(button, state, x, y);
	}
	static void motionCallback(int x, int y)
	{
		instance().motion(x, y);
	}
	static void idleCallback()
	{
		instance().idle();
	}
	static void keyboardCallback(unsigned char key, int x, int y)
	{
		instance().keyboard(key, x, y);
	}

	void display() const;
	void reshape(int width, int height);
	void mouse(int button, int state, int x, int y);
	void motion(int x, int y);
	void idle();
	void keyboard(unsigned char key, int x, int y);

	GLuint m_reliefProgram, m_debugProgram, m_computeProgram;
	GLuint fboID, color_rboID, depth_rboID;
	float zNear = 1;
	float zFar = 20;
	float fovy = (float) M_PI_2;
	int screenWidth, screenHeight;

	std::string shaderName;

	std::vector<ReliefMesh *> objects;

	glm::mat4 viewMat = glm::lookAt(glm::vec3(0, 0, 1), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	glm::mat4 projMat = glm::perspective<float>((float) M_PI_2, 1.f, zNear, zFar);

	Arcball arcball;

	FPSCounter fps;

	static EStatus status;
};

#endif
