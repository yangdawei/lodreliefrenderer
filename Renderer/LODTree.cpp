#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <algorithm>
#include <bitset>
#include <queue>

#include "LODTree.h"
#include "../include/ImageHelper.h"
#include "../include/GLEW/glew.h"

using namespace std; 
using namespace glm;

void LODTree::load(string folderName, string texPngFile, int treeHeight, int hWidth, int hHeight)
{
	this->treeHeight = treeHeight;
	this->hWidth = hWidth;
	this->hHeight = hHeight;

	// Read color texture
	loadColorPNG(texture, texPngFile.c_str());

	// Read height maps
	char namebuffer[4096];
	char namebufferTest[4096];
	for (size_t i = 0; i < length() - 1; i++)
	{
		Image1f *image = new Image1f(hWidth, hHeight);
		sprintf(namebuffer, "%s/%lu.dat", folderName.c_str(), i);
		// sprintf(namebuffer, "%s/0.dat", folderName.c_str());
		sprintf(namebufferTest, "%s/%lu_test.dat", folderName.c_str(), i);
		image->readRaw(namebuffer);
		image->saveRaw(namebufferTest);

		// TODO:
		for (int i = 0; i < image->height(); i++)
		for (int j = 0; j < image->width(); j++)
			image->set(j, i, image->get(j, i) / 4.f);

		heightMaps.push_back(image);
	}

	// Add a debug map
	Image1f *image = new Image1f(hWidth, hHeight);
	for (int i = 0; i < image->height(); i++)
	for (int j = 0; j < image->width(); j++)
		image->set(j, i, 1);
	heightMaps.push_back(image);

	invIndices.resize(depth());
	texArrIndices.resize(length());

	block = Block(depth());
}

void LODTree::init()
{
	// Load the debug map.
	set<glm::uint> indices;
	indices.insert(length() - 1);
	update(indices);
	curSet.clear();

	int pos = texArrIndices[length() - 1];
	texArrIndices.assign(length(), pos);

	for (auto iter = modifiedSet.begin(); iter != modifiedSet.end(); iter++)
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, GLint(*iter), width(), height(), 1,
			GL_RED, GL_FLOAT, heightMapAt(inverseIndices()[*iter]));
}

void LODTree::update(set<glm::uint> &indices)
{
	modifiedSet.clear();

	// set<glm::uint> newSet(indices.begin(), indices.end());
	set<glm::uint> &newSet = indices;

	// Find the added nodes and deleted nodes(ID)
	set<glm::uint> added, deleted;
	set_difference(newSet.begin(), newSet.end(), curSet.begin(), curSet.end(), inserter(added, added.begin()));
	set_difference(curSet.begin(), curSet.end(), newSet.begin(), newSet.end(), inserter(deleted, deleted.begin()));

	auto addedIt = added.begin();
	auto deletIt = deleted.begin();

	// Some deleted nodes can be regarded as 'update' for added nodes
	while (addedIt != added.end() && deletIt != deleted.end())
	{
		glm::uint tmp = texArrIndices[*addedIt] = texArrIndices[*deletIt];
		texArrIndices[*deletIt] = 0;
		invIndices[tmp] = *addedIt;
		modifiedSet.insert(tmp);

		addedIt++;
		deletIt++;
	}

	// There are still nodes to delete
	while (deletIt != deleted.end())
	{
		block.free(texArrIndices[*deletIt]);
		glm::uint tmp = texArrIndices[*deletIt];
		texArrIndices[*deletIt] = 0;
		invIndices[tmp] = texArrIndices[length() - 1];
		deletIt++;
	}

	// There are still nodes to add
	while (addedIt != added.end())
	{
		glm::uint tmp = texArrIndices[*addedIt] = (unsigned int)block.alloc();
		invIndices[tmp] = *addedIt;
		modifiedSet.insert(tmp);
		addedIt++;
	}

	for (size_t i = 0; i < invIndices.size(); i++)
		printf("%lu ", invIndices[i]);
	puts("");

	for (size_t i = 0; i < texArrIndices.size(); i++)
		printf("%lu ", texArrIndices[i]);
	puts("");
	puts("==============");

	curSet = newSet;
}
