#version 430

#define MAX_OBJECT_NUM 5;

layout(triangles) in;
layout(triangle_strip, max_vertices=11) out;

uniform mat4 mModelView;
uniform mat4 mProject;
uniform int status;

in vec3 vVertex[];
in vec4 vNormalMaxHeight[];
in vec2 vUV[];

smooth out vec3 gVertex;
smooth out vec2 gUV;
flat out vec3 gVertices[3];
flat out vec2 gCoords[3];

flat out vec4 gNormalMaxHeight;
flat out float gTransA;
flat out uint gLevel;

layout(binding=1) uniform usampler1D treeTexArr;
layout(binding=2) uniform usampler1D feedback;

void main()
{
	if (status != 2)
	{
		gLevel = texelFetch(feedback, gl_PrimitiveIDIn, 0).x;
		gTransA = 1 << gLevel;
	}
	else
	{
		gLevel = 0;
		gTransA = 1.0;
	}

	// The lower and upper 3 vertices
	vec4 pOrigin[3];
	vec4 pDispl[3];

	vec3 origin[3];
	vec3 displ[3];
	vec3 originNormal;
	vec4 tmp;
	float height;

	gNormalMaxHeight = vNormalMaxHeight[0];
	originNormal = vNormalMaxHeight[0].xyz;
	height = vNormalMaxHeight[0].w;

	for (int i = 0; i < 3; i++)
	{
		tmp = mModelView * vec4(vVertex[i], 1);
		origin[i] = tmp.xyz / tmp.w;
		pOrigin[i] = mProject * vec4(origin[i], 1);


		tmp = mModelView * vec4(vVertex[i]
			+ height * originNormal, 1);
		displ[i] = tmp.xyz / tmp.w;
		pDispl[i] = mProject * vec4(displ[i], 1);
	}

	// Interpolated and Uninterpolated transformed vertex position
	// Top face
	for (int i = 0; i < 3; i++)
	{
		gVertices[i] = origin[i];
		gCoords[i] = vUV[i];
	}
	for (int i = 0; i < 3; i++)
	{
		gl_Position = pDispl[i];
		gl_PrimitiveID = gl_PrimitiveIDIn;
		gVertex = displ[i];
		gUV = vUV[i];

		EmitVertex();
	}
	EndPrimitive();

	int idx = 0;
	for (int i = 0; i < 4; i++)
	{
		int i_0 = (i + 0) % 3;
		int i_1 = (i + 1) % 3;
		int i_2 = (i + 2) % 3;

		gl_Position = pDispl[i_0];
		gVertex = displ[i_0];
		gUV = vUV[i_0];
		gl_PrimitiveID = gl_PrimitiveIDIn;

		idx = (idx + 1) % 3;
		EmitVertex();

		gl_Position = pOrigin[i_0];
		gVertex = origin[i_0];
		gUV = vUV[i_0];
		gl_PrimitiveID = gl_PrimitiveIDIn;

		idx = (idx + 1) % 3;
		EmitVertex();
	}

	EndPrimitive();
}
