#version 430 core
#define MY_ARRAY_SIZE 2000
// #define FLOAT_FEEDBACK;

smooth in vec3 gVertex;
smooth in vec2 gUV;
flat in vec3 gVertices[3];
flat in vec2 gCoords[3];

// Texture transform parameters
flat in vec4 gNormalMaxHeight;

flat in uint gLevel;
flat in float gTransA;

out vec4 fColor;

uniform mat4 mModelView;
uniform mat4 mView;
uniform int status;

// Final 2d-array texture
layout(binding=0) uniform sampler2DArray heightMapLOD;

layout(binding=1) uniform usampler1D treeTexArr;

layout(binding=2) uniform usampler1D feedback;
layout(binding=3) uniform sampler2D colorTexture;

#define WHITE vec4(1, 1, 1, 1)
#define GRAY vec4(0.5, 0.5, 0.5, 1)
#define RED vec4(1, 0, 0, 1)
#define GREEN vec4(0, 1, 0, 1)
#define BLUE vec4(0, 0, 1, 1)
#define YELLOW vec4(1, 1, 0, 1)
#define CYAN vec4(0, 1, 1, 1)
#define PURPLE vec4(1, 0, 1, 1)

#define NOT_A_FACE -1
#define BOTTOM_FACE 0
#define SIDE_FACE1 1
#define SIDE_FACE2 2
#define SIDE_FACE3 3
#define TOP_FACE   4

#define EPSILON 0.00001
#define STEP_SIZE 32

#define ASSERT(expr)  \
{ \
	if (!(expr)) \
	{\
		fColor = RED;\
		return; \
	}\
}

#define RETURN(color) { fColor = color; return; }


vec3 hue2rgb(float h)
{
	float v = 0.9;
	float s = 0.9;
	if (h >= 1 || h < 0)
		return vec3(1, 1, 1);

	int h_i = int(h * 6);
	float f = h * 6 - h_i;
	float p = v * (1 - s);
	float q = v * (1 - f * s);
	float t = v * (1 - (1 - f) * s);

	if (h_i == 0)
		return vec3(v, t, p);
	else if (h_i == 1)
		return vec3(q, v, p);
	else if (h_i == 2)
		return vec3(p, v, t);
	else if (h_i == 3)
		return vec3(p, q, v);
	else if (h_i == 4)
		return vec3(t, p, v);
	else if (h_i == 5)
		return vec3(v, p, q);
	else
		return vec3(1, 1, 1);
}

vec4 intersect(vec3 v, mat3 rMat)
{
	vec3 k = inverse(rMat) * v;
	float lambda = 1 / (k[0] + k[1] + k[2]);
	return vec4(lambda * k, lambda);
}

vec3 proj_barycentric_coord(vec3 p, mat3 invMat, vec3 n, vec3 p0)
{
	vec3 pp = p - dot(p - p0, n) * n;
	return invMat * pp;
}

float prism_intersect(vec3 origin, vec3 dir, vec3 p1, vec3 p2, vec3 p3, vec3 n, float maxHeight,
	out int faceID)
{
	// Intersection test with bottom
	vec4 lambda = intersect(dir, mat3(p1 - origin, p2 - origin, p3 - origin));
	if (lambda.x >= 0 && lambda.y >= 0 && lambda.z >= 0 && lambda.w >= 0)
	{
		faceID = BOTTOM_FACE;
		return lambda.w;
	}

	// Intersection test with side faces
	vec3 parr[3];
	parr[0] = p1;
	parr[1] = p2;
	parr[2] = p3;
	for (int i = 0; i < 3; i++)
	{
		int i_1 = (i + 1) % 3;
		vec3 lambdamiut = inverse(mat3(parr[i_1] - parr[i], n, -dir)) * (origin - parr[i]);
		if (lambdamiut.x >= 0 && lambdamiut.x <= 1
			&& lambdamiut.y >= 0 && lambdamiut.y <= maxHeight
			&& lambdamiut.z >= EPSILON)
		{
			faceID = i + SIDE_FACE1;
			return lambdamiut.z;
		}
	}

	// Intersection test with top
	vec3 displ = n * maxHeight;
	lambda = intersect(dir,
		mat3(p1 - origin + displ, p2 - origin + displ, p3 - origin + displ));
	if (lambda.x >= 0 && lambda.y >= 0 && lambda.z >= 0 && lambda.w >= 0)
	{
		faceID = TOP_FACE;
		return lambda.w;
	}
	faceID = NOT_A_FACE;
	return lambda.w;
}

vec2 coords_interpolate(vec3 pos, mat3 invMat, vec3 n, vec3 p0, vec2 coords0, vec2 coords1, vec2 coords2)
{
	vec3 lambda = proj_barycentric_coord(pos, invMat, n, p0);
	vec2 coordsIntp = lambda[0] * coords0 + lambda[1] * coords1 + lambda[2] * coords2;
	return coordsIntp;
}

void computeTransParams(vec2 coord, out vec2 transCoord, out uint lodIdx)
{
	vec2 uv = coord;
	int idx = 0;
	for (int i = 0; i < gLevel; i++)
	{
		ivec2 shift = ivec2(0, 0);
		if (uv.s >= 0.5) shift.s = 1;
		if (uv.t >= 0.5) shift.t = 1;
		idx = idx * 4 + shift.t * 2 + shift.s + 1;
		uv = 2 * uv - shift;
	}
	lodIdx = texelFetch(treeTexArr, idx, 0).x;
	transCoord = uv;
}

bool height_test(vec3 pos, mat3 invMat, vec3 n, vec3 p0, vec2 coords0, vec2 coords1, vec2 coords2)
{
	vec2 coordsIntp = coords_interpolate(pos, invMat, n, p0, coords0, coords1, coords2);
	vec2 transCoord; uint lodIndex;
	computeTransParams(coordsIntp, transCoord, lodIndex);
	return (dot(pos - p0, n) <= texture(heightMapLOD, vec3(transCoord, lodIndex)).r);
}

void main()
{
	vec3 lightDir = normalize(mModelView * vec4(1, 0.2, 0.3, 0)).xyz;

	// Transform to ModelView coordinates
	vec3 normal = gNormalMaxHeight.xyz;
	float maxHeight = gNormalMaxHeight.w;

	normal = normalize((mModelView * vec4(normal, 0)).xyz);

	// Find the intersection with the triangular prism
	vec3 viewDir = normalize(gVertex);
	int faceID;
	float tau = prism_intersect(gVertex + EPSILON * viewDir, viewDir,
		gVertices[0], gVertices[1], gVertices[2], normal, maxHeight, faceID);

	if (status == 4)
	{
		fColor = texture(colorTexture, gUV) * (max(dot(normal, lightDir), 0) + 0.5);
		if (faceID == BOTTOM_FACE)
			return;
		else
			discard;
	}

	if (status == 3)
	{
	switch(faceID)
	{
		case(BOTTOM_FACE):
			RETURN(WHITE);
		case(SIDE_FACE1):
			RETURN(RED);
		case(SIDE_FACE2):
			RETURN(GREEN);
		case(SIDE_FACE3):
			RETURN(BLUE);
		case(TOP_FACE):
			RETURN(GRAY);
		case(NOT_A_FACE):
			RETURN(YELLOW);
		}
		discard;
	}


	if (faceID == NOT_A_FACE)
		discard;
	// tau += EPSILON;

	// Iterate through the line to find intersection
	// To avoid collinear when eye is center
	vec3 newCenter = gVertices[0] + normal;
	mat3 invMat = inverse(mat3(gVertices[0] - newCenter, gVertices[1] - newCenter,
		gVertices[2] - newCenter));

	float tStep = tau / STEP_SIZE;
	vec3 vStep = viewDir * tau / STEP_SIZE;

	vec3 current = gVertex + vStep + viewDir * EPSILON - newCenter;
	fColor = WHITE;

	vec3 start, end;
	int loops;

	bool found = false;
	for (int i = 1; i <= STEP_SIZE; i++, current += vStep)
	{
		found = height_test(current, invMat, normal, -normal, gCoords[0], gCoords[1], gCoords[2]);
		if (found)
			break;
	}
	if (!found)
		discard;

	start = current - vStep;
	end = current;
	loops = 8;

	for (int i = 0; i < loops; i++)
	{
		vec3 mid = (start + end) / 2;
		if (height_test(mid, invMat, normal, -normal, gCoords[0], gCoords[1], gCoords[2]))
			end = mid;
		else
			start = mid;
	}

	vec3 finalPos = (start + end) / 2;

	vec2 colorTexUV = coords_interpolate(finalPos, invMat, normal, -normal, gCoords[0], gCoords[1], gCoords[2]);

	// Compute normal
	vec3 proj[3];
	vec2 coordsIntp[3];
	float h[3];
	vec3 projPlusH[3];
	for (int i = 0; i < 3; i++)
	{
		proj[i] = finalPos + 0.1 * (gVertices[i] - gVertices[0]);//  / (1 << gLevel);
		coordsIntp[i] = coords_interpolate(proj[i], invMat, normal, -normal, gCoords[0], gCoords[1], gCoords[2]);

		vec2 transCoord; uint lodIdx;
		computeTransParams(coordsIntp[i], transCoord, lodIdx);

		h[i] = texture(heightMapLOD, vec3(transCoord, lodIdx)).r;
		projPlusH[i] = proj[i] + h[i] * normal;
	}

	vec3 minus1 = projPlusH[1] - projPlusH[0];
	vec3 minus2 = projPlusH[2] - projPlusH[0];
	vec3 localNormal = cross(minus1, minus2);
	if (length(localNormal) == 0)
		localNormal = normal;
	else
		localNormal = normalize(localNormal);

	fColor = texture(colorTexture, colorTexUV) * (max(dot(localNormal, lightDir), 0) + 0.5);
	// fColor = texture(colorTexture, colorTexUV);
	if (status == 1)
	{
		if (gLevel == 0)
			RETURN(BLUE)
		else if (gLevel == 1)
			RETURN(GREEN)
		else if (gLevel == 2)
			RETURN(YELLOW)
		else
			RETURN(RED)
	}
	else if (status == 2)
	{
		float level = texelFetch(feedback, gl_PrimitiveID, 0).x / 5000000.0;
		fColor = vec4(level);
	}
}
