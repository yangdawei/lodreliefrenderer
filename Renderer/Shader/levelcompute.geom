#version 430

#define FLT_MAX 1e6
#define INT_MAX 1e5

layout(triangles) in;
layout(points, max_vertices=1) out;

in vec3 vVertex[];
in vec4 vNormalMaxHeight[];
in vec2 vUV[];
in vec4 vErr0[];
in vec4 vErr1[];

uniform mat4 mModelView;
uniform mat4 mProject;
uniform float predRadius;
uniform float predAccu;

uniform int treeHeight;
uniform int status;

#ifndef FLOAT_FEEDBACK
out uint level;
#else
out float level;
#endif

mat3 rotate(vec3 v, float angle)
{
	float a = angle;

	float c = cos(a);
	float s = sin(a);

	vec3 axis = normalize(v);
	vec3 temp = (float(1) - c) * axis;

	mat3 result;
	result[0][0] = c + temp[0] * axis[0];
	result[0][1] = 0 + temp[0] * axis[1] + s * axis[2];
	result[0][2] = 0 + temp[0] * axis[2] - s * axis[1];

	result[1][0] = 0 + temp[1] * axis[0] - s * axis[2];
	result[1][1] = c + temp[1] * axis[1];
	result[1][2] = 0 + temp[1] * axis[2] + s * axis[0];

	result[2][0] = 0 + temp[2] * axis[0] + s * axis[1];
	result[2][1] = 0 + temp[2] * axis[1] - s * axis[0];
	result[2][2] = c + temp[2] * axis[2];

	return result;
}

#define RETURN { EmitVertex(); EndPrimitive(); return; }

void main()
{
	// Sample 7 points
	float bound[7] = {0, 0, 0, 0, 0, 0, 0};
	vec4 tmp = vNormalMaxHeight[0];
	vec3 n = normalize((mModelView * vec4(tmp.xyz, 0)).xyz);
	float height = tmp.w;

	vec3 vVertices[7] = { vVertex[0], vVertex[1], vVertex[2],
		vVertex[0] + height * n, vVertex[1] + height * n, vVertex[2] + height * n, vec3(0, 0, 0)};
	vVertices[6] = (vVertex[0] + vVertex[1] + vVertex[2]) / 3 + 0.5 * height * n;

	for (int i = 0; i < 7; i++)
	{
		vec3 v = vVertices[i];
		float lv = length(v);
		if (lv <= predRadius)
		{
			bound[i] = FLT_MAX;
			break;
		}
		float lmin = lv - predRadius;
		float th = asin(predRadius / lv);
		vec3 a = cross(v, n);
		if (length(a) == 0)
		{
			bound[i] = 1 / FLT_MAX;
			continue;
		}
		vec3 l1 = -rotate(a, th) * v;
		vec3 l2 = -rotate(a, -th) * v;
		float d1 = dot(l1, n);
		float d2 = dot(l2, n);
		if (d1 * d2 <= 0)
			bound[i] = 1 / lmin;
		else if (d1 < 0)
			bound[i] = sqrt(1 - pow(min(abs(d1), abs(d2)), 2) / pow(lv, 2)) / lmin;
		else
			bound[i] = 1 / FLT_MAX;
			continue;
	}

	
	// Find the max theta

	float maxRatio = 0;
	for (int i = 0; i < 7; i++)
		maxRatio = max(maxRatio, bound[i]);

	float allowedError = predAccu / maxRatio;

	if (status == 2)
	{
		level = int(INT_MAX / allowedError);
		RETURN;
	}

	int i;
	vec4 err0 = vErr0[0];
	vec4 err1 = vErr1[0];
		
	if (treeHeight - 1 <= 4)
	{
		for (i = 0; i < treeHeight - 1; i++)
		{
			if (err0[i] < allowedError)
				break;
		}
	}
	else
	{
		for (i = 0; i < 4; i++)
		{
			if (err0[i] < allowedError)
				break;
		}
		if (i == 4)
		{
			for (i = 4; i < treeHeight - 1; i++)
			{
				if (err1[i - 4] < allowedError)
					break;
			}
		}
	}
	level = i;
	RETURN;
}
