#version 400 core
//#extension GL_ARB_explicit_uniform_location : require

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec4 inNormalMaxHeight;
layout(location = 2) in vec2 inUV;
layout(location = 3) in vec4 inErr0;
layout(location = 4) in vec4 inErr1;

smooth out vec3 vVertex;
smooth out vec4 vNormalMaxHeight;
smooth out vec2 vUV;
smooth out vec4 vErr0;
smooth out vec4 vErr1;

void main()
{
    vec4 inPosition_ = vec4(inPosition, 1);

	gl_Position = inPosition_;

	vVertex = inPosition;
	vNormalMaxHeight = inNormalMaxHeight;
	vUV = inUV;
	vErr0 = inErr0;
	vErr1 = inErr1;
}
