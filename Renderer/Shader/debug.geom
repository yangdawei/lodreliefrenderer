#version 430

#define FLT_MAX 1e6

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 vVertex[];
in vec4 vNormalMaxHeight[];
in vec2 vUV[];
in vec4 vErr0[];
in vec4 vErr1[];

out vec4 gErr0;
out vec4 gErr1;

uniform mat4 mModelView;
uniform mat4 mProject;

uniform int treeHeight;

in vec3 vVertex[];

void main()
{
	vec4 pOrigin[3];
	vec3 origin[3];
	vec3 originNormal;

	vec4 tmp;

	originNormal = vNormalMaxHeight[0].xyz;

	for (int i = 0; i < 3; i++)
	{
		tmp = mModelView * vec4(vVertex[i], 1);
		origin[i] = tmp.xyz / tmp.w;
		pOrigin[i] = mProject * vec4(origin[i], 1);
	}

	for (int i = 0; i < 3; i++)
	{
		gl_Position = pOrigin[i];
		gl_PrimitiveID = gl_PrimitiveIDIn;
		gErr0 = vErr0[0];
		gErr1 = vErr1[0];

		EmitVertex();
	}
	EndPrimitive();
}
