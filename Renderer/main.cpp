#include <iostream>

#include "Renderer.h"

using namespace std;

int main()
{

	Renderer &renderer = Renderer::instance();

	renderer.loadScene("../Renderer/Scene/example.json");

	renderer.init();
	renderer.startLoop();
	renderer.release();

	return 0;
}
